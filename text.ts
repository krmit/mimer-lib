"use strict";
import colors from "colors/safe";

/**
 * Shorhand function to easy create a Text objet.
 *
 * @params messages A list of strings or [[Msg]]
 * @return A new text that can be modified by the methods of Text.
 */

export function t(...messages: any) {
  return new Text(messages);
}

/**
 * The Text class represent text with styles. It contains both string
 * and [[Msg]].
 */

function doSpan(text: string, length: number, span: number): string {
  if (span < length) {
    if (length > 6) {
      return text.substring(0, length - 3) + "..."; // This will be buggy if text contains escape charaters.
    } else {
      return text.substring(0, length);
    }
  } else if (span !== length) {
    return text + " ".repeat(span - length);
  } else {
    return text;
  }
}

function doFix(text: string, fix: number): string {
  let result = Number(text);
  if (!isNaN(result)) {
    return String(result.toFixed(fix));
  } else {
    return text;
  }
}

function doPercent(text: string) {
  let result = Number(text);
  if (!isNaN(result)) {
    return String(result * 100) + "%";
  } else {
    return text;
  }
}

export class Text {
  private color?: string;
  private fontWeight?: string;
  private textDecoration?: string;
  private theme?: string;
  private tags: string[] = [];
  private data: (string | Text)[];
  private span = 0;
  length = 0;
  private fix = -1;
  private percent = false;
  private newline = false;
  private newlinebefor = false;
  private orderedlist = false;
  private isList = false;

  /**
   * Create a Text from parameters.
   *
   * @param messages A list of strings or [[Msg]]
   */

  constructor(messages: (string | Text)[] | string = []) {
    this.data = [];
    this.tags = [];
    this.newline = false;

    if (typeof messages === "string") {
      this.data[0] = messages;
      this.length = messages.length;
    } else {
      for (let message of messages) {
        this.add(message);
      }
    }
  }

  add(message: string | Text) {
    this.data.push(message);
    if (message instanceof Text) {
      Object.setPrototypeOf(message, this); // This should be done in the converting functionsinsted. Since this could give subtile errors.
    }
    this.length += message.length;
    return this;
  }

  protected get getData(): (string | Text)[] {
    return this.data;
  }

  get toText(): string {
    return this.data.reduce<string>((text, message) => {
      let result = "";
      if (message instanceof Text) {
        result += message.toText;
        if (message.span > 0) {
          result += doSpan(result, message.length, message.span) + "\n";
        }
        if (message.fix >= 0) {
          result = doFix(result, message.fix);
        }
        if (message.newline) {
          result += result + "\n";
        }
      } else {
        result = message;
      }
      return text + "" + result;
    }, "");
  }

  get toJSON(): any {
    const result: any = {};
    if (this.color) {
      result["color"] = this.color;
    }
    if (this.fontWeight) {
      result["fontWeight"] = this.fontWeight;
    }
    if (this.theme) {
      result["theme"] = this.theme;
    }
    if (this.newline === true) {
      result.newline = true;
    }
    if (this.tags.length > 0) {
      result.tags = Object.assign([], this.tags);
    }
    result.data = [];
    for (let item of this.data) {
      if (item instanceof Text) {
        result.data.push(item.toJSON);
      } else {
        result.data.push(item);
      }
    }

    return result;
  }

  get toTerminal(): string {
    let index = 0;
    let r = this.data.reduce<string>((text, message) => {
      index++;
      let result = "";
      if (message instanceof Text) {
        result += message.toTerminal;
      } else {
        result = message;
        if (this.fix >= 0) {
          result = doFix(result, this.fix);
        }
        if (this.percent) {
          result = doPercent(result);
        }
        if (this.color) {
          result = (colors as any)[this.color as string](result) as string;
        }
        if (this.fontWeight) {
          result = (colors as any)[this.fontWeight as string](result) as string;
        }
        if (this.textDecoration) {
          if (this.textDecoration === "line-through") {
            result = colors["strikethrough"](result) as string;
          }
          if (this.textDecoration === "overline") {
            // No such thing in a terminal.
          } else {
            result = (colors as any)[this.textDecoration as string](
              result
            ) as string;
          }
        }
      }
      if (this.orderedlist) {
        result = "  " + colors.bold(index + ".") + "  " + result + "\n";
      }
      if (this.isList) {
        result = result + "\n";
      }
      return text + "" + result;
    }, "");
    if (this.span > 0) {
      r = doSpan(r, this.length, this.span);
    }
    if (this.newline) {
      r += "\n";
    }
    if (this.newlinebefor) {
      r = "\n" + r;
    }
    return r;
  }

  setTheme(themeName: string) {
    this.theme = themeName;
    return this;
  }

  setSpan(span: number) {
    this.span = span;
    return this;
  }

  setFix(fix: number) {
    this.fix = fix;
    return this;
  }

  addTag(tag: string) {
    this.tags.push(tag);
    return this;
  }

  hasTag(tag: string) {
    return this.tags.indexOf(tag) >= 0;
  }

  get isPercent() {
    this.percent = true;
    return this;
  }

  get newLine() {
    this.newline = true;
    return this;
  }

  get newLineBefor() {
    this.newlinebefor = true;
    return this;
  }

  get orderedList() {
    this.orderedlist = true;
    return this;
  }

  get list() {
    this.isList = true;
    return this;
  }

  get headline() {
    this.tags.push("headline");
    this.textDecoration = "underline";
    this.fontWeight = "bold";
    this.newline = true;
    this.newlinebefor = true;
    return this;
  }

  get importent() {
    this.tags.push("important");
    this.color = "red";
    this.fontWeight = "bold";
    return this;
  }

  get red() {
    this.color = "red";
    return this;
  }

  get blue() {
    this.color = "blue";
    return this;
  }

  get green() {
    this.color = "green";
    return this;
  }

  get yellow() {
    this.color = "yellow";
    return this;
  }

  get magenta() {
    this.color = "magenta";
    return this;
  }

  get cyan() {
    this.color = "cyan";
    return this;
  }

  get gray() {
    this.color = "gray";
    return this;
  }

  get bold() {
    this.fontWeight = "bold";
    return this;
  }

  get underline() {
    this.textDecoration = "underline";
    return this;
  }

  get lineThrough() {
    this.textDecoration = "line-through";
    return this;
  }

  get overline() {
    this.textDecoration = "overline";
    return this;
  }
}
