"use strict";
import colors from "colors/safe";

/**
 * Shorhand function to easy create a Text objet.
 *
 * @params messages A list of strings or [[Msg]]
 * @return A new text that can be modified by the methods of Text.
 */

export function serverError(req:any, res:any, msg:string, log:any) {
  log.error("404: Not found. " + req.url);
  let result = "<h1>Not Found: 404</h1><h2>" + req.url + "<h2>";
  
  if(msg) {
      result += "<h2>"+msg + "</h2>";
  }
  
  res.status(404).send(result);
}
